package com.example.wakecab.ui.home;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wakecab.R;
import com.example.wakecab.utils.ApiHandler;
import com.example.wakecab.utils.AsyncApi;
import com.example.wakecab.utils.DividerItemDecoration;
import com.example.wakecab.utils.CommonUtils;
import com.example.wakecab.utils.Preference_DriverProfile;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class HomeFragment extends Fragment implements AsyncApi {

    private GoogleMap mMap;
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private ApiHandler api;
    private RecyclerView.LayoutManager manager;
    private SpinKitView loadView;
    private HomeViewModel homeViewModel;
    private Preference_DriverProfile preDriver;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        loadView = root.findViewById(R.id.spin_kit_home);
        preDriver = Preference_DriverProfile.getInstance(this.getActivity());
        NavController navController = Navigation.findNavController(container);

        if(preDriver.getLoged() == false ){
            navController.popBackStack();
            navController.navigate(R.id.nav_gallery);
            return null;
        }

        if(preDriver.getIsactive()) {
            recyclerView = root.findViewById(R.id.recicler);
            this.api = new ApiHandler(this);
            this.api.getcontext(getContext());
            this.api.gettickets();
            mLayoutManager = new LinearLayoutManager(getActivity());
            mLayoutManager.setOrientation(RecyclerView.VERTICAL);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            Drawable dividerDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.line_divider);
            manager = new LinearLayoutManager(getActivity());

            loadView.setVisibility(View.VISIBLE);
        }else {
            Snackbar.make(root,"Ususario no activo hable con el administrdor",Snackbar.LENGTH_LONG).show();
        }
        return root;
    }


    private void prepareDemoContent() {
        new Handler().postDelayed(() -> {
            //prepare data and show loading
            CommonUtils.hideLoading();
        }, 2000);
        Toast.makeText(getActivity(),this.recyclerView.getAdapter().getItemCount()+" ",Toast.LENGTH_LONG).show();
    }


    public void tkReady(ArrayList<TicketsBean> tk){
        TicketsAdapter tks = new TicketsAdapter(getActivity(), tk);


        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(tks);
        recyclerView.setRecyclerListener(mRecycleListener);
        loadView.setVisibility(View.INVISIBLE);

    }
    private RecyclerView.RecyclerListener mRecycleListener = new RecyclerView.RecyclerListener() {

        @Override
        public void onViewRecycled(RecyclerView.ViewHolder holder) {
            TicketsAdapter.ViewHolder mapHolder = (TicketsAdapter.ViewHolder) holder;
            if (mapHolder != null && mapHolder.map != null) {
                // Clear the map and free up resources by changing the map type to none.
                // Also reset the map when it gets reattached to layout, so the previous map would
                // not be displayed.
                mapHolder.map.clear();
                mapHolder.map.setMapType(GoogleMap.MAP_TYPE_NONE);
            }
        }
    };



}