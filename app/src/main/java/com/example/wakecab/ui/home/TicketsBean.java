package com.example.wakecab.ui.home;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

public class TicketsBean {

    @SerializedName("email")
    private String email;
    @SerializedName("telefono")
    private String telefono;
    @SerializedName("tarifa")
    private String tarifa;
    @SerializedName("precio")
    private String precio;
    @SerializedName("id")
    private String id;
    @SerializedName("serial")
    private String serial;
    @SerializedName("date")
    private ZonedDateTime date;
    @SerializedName("client")
    private String client;
    @SerializedName("driver")
    private String driver;
    @SerializedName("maletas")
    private String maletas;
    @SerializedName("cartype")
    private String carTipe;
    @SerializedName("company")
    private String company;
    @SerializedName("state")
    private String state;
    @SerializedName("personas")
    private String personas;
    @SerializedName("babyseat")
    private String babySeat;
    @SerializedName("disable")
    private String disable;
    @SerializedName("direccion")
    private String direccion;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;
    @SerializedName("nameloc")
    NamedLocation namedLocation;


    public TicketsBean(){


    }

    public NamedLocation getNamedLocation() {

        return new NamedLocation(this.direccion,new LatLng(Double.parseDouble(this.lat),Double.parseDouble(this.lng)));
    }

    public void setNamedLocation(NamedLocation namedLocation) {

        this.namedLocation = namedLocation;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getBabySeat() {
        return babySeat;
    }

    public void setBabySeat(String babySeat) {
        this.babySeat = babySeat;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getMaletas() {
        return maletas;
    }

    public void setMaletas(String maletas) {
        this.maletas = maletas;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }



    public String getCarTipe() {
        return carTipe;
    }

    public void setCarTipe(String carTipe) {
        this.carTipe = carTipe;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPersonas() {
        return personas;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTarifa() {
        return tarifa;
    }

    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }


    public void setPersonas(String personas) {
        this.personas = personas;
    }

    public  class NamedLocation {

        public final String name;
        public final LatLng location;

        NamedLocation(String name, LatLng location) {
            this.name = name;
            this.location = location;
        }
    }
}
