package com.example.wakecab.ui.login;

public interface LoginPresenter {

    /*when user click on login button from Activity*/
    void login(String username, String password);

}
