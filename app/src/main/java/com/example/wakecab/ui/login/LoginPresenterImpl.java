package com.example.wakecab.ui.login;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.wakecab.utils.ApiHandler;
import com.example.wakecab.utils.Preference_DriverProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginPresenterImpl implements LoginPresenter {

    public Context ctx;
    private Send send;
    private Preference_DriverProfile preDriver;
    private LoginView loginView;


    public void getCtx(Context ctx){
        this.ctx = ctx;
        this.preDriver = Preference_DriverProfile.getInstance(ctx);

    }
    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void login(String username, String password) {
        if ((TextUtils.isEmpty(username) || TextUtils.isEmpty(password))) {
            loginView.showValidationErrorMsg("Los campos estan vacios");
        } else {
            this.send = new Send();
            this.send.authGson(username, password);
        }
    }




    class Send {
        RequestQueue queue;
        String url_auth = "https://wakecabtest.xyz/api/android/auth/";
        String url_tickets = "https://wakecabtest.xyz/en/api/android/tickets/";




        public void authGson(final String username, final String password) {
            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put("username", username);
                jsonBody.put("password", password);
            } catch (JSONException e) {


            }

            MySingleton.getInstance(ctx.getApplicationContext()).getRequestQueue();
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                        (Request.Method.POST, url_auth, jsonBody, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                preDriver.putWakecabuser(new WakecabUser(username, password));
                                preDriver.putNickname(username);

                                Toast.makeText(ctx.getApplicationContext(),"token",Toast.LENGTH_LONG).show();
                                try {
                                    preDriver.putToken(response.get("token").toString());
                                    preDriver.putVisits(preDriver.getVisits());
                                    preDriver.putLoged(true);
                                    preDriver.putId(response.get("id").toString());
                                    preDriver.putIsactive(response.getBoolean("is_active"));
                                    preDriver.putUseremail(response.get("user_email").toString());


                                } catch (JSONException e) {
                                    Toast.makeText(ctx.getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }
                                loginView.loginSuccessFully(response.toString());
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO: Handle error
                                loginView.loginFail(error.networkResponse.toString());
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        final Map<String, String> headers = new HashMap<>();
                        // headers.put("Authorization", "Token " + "c2FnYXJAa2FydHBheS5jb206cnMwM2UxQUp5RnQzNkQ5NDBxbjNmUDgzNVE3STAyNzI=");//put your token here
                        return headers;

                    }

                };
                 MySingleton.getInstance(ctx).addToRequestQueue(jsonObjectRequest);



        }




    }
}
