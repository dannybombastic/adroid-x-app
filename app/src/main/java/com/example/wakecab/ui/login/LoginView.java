package com.example.wakecab.ui.login;

public interface LoginView {


    void showValidationErrorMsg(String msg);
    void loginSuccessFully(String msg);
    void loginFail(String msg);
}
