package com.example.wakecab.ui.login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.wakecab.MainActivity;
import com.example.wakecab.R;
import com.example.wakecab.ui.home.HomeFragment;
import com.example.wakecab.ui.share.ShareFragment;
import com.example.wakecab.utils.Preference_DriverProfile;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.material.navigation.NavigationView;

public class LoginWakecabFragment extends Fragment implements LoginView{
    private SpinKitView loadView;
    private Button login;
    private EditText email,password;
    private LoginWakecabViewModel loginWakecabViewModel;
    private Preference_DriverProfile preDriver;
    NavigationView navigationView;
    private LoginPresenterImpl  hanlderLogin = new LoginPresenterImpl(this);

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        loginWakecabViewModel =
                ViewModelProviders.of(this).get(LoginWakecabViewModel.class);
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        NavController navController = Navigation.findNavController(container);
        preDriver = Preference_DriverProfile.getInstance(this.getActivity());
        if(preDriver.getLoged()){
            navController.popBackStack();
            navController.navigate(R.id.nav_home);

        }
        final TextView textView = root.findViewById(R.id.text_gallery);
        email = root.findViewById(R.id.user_email);
        loadView = root.findViewById(R.id.spin_kit);
        loadView.setVisibility(View.INVISIBLE);
        password = root.findViewById(R.id.user_password);
        login =  root.findViewById(R.id.login_button);
        navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);




        hanlderLogin.getCtx(this.getActivity());
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validatePermission()){

                    loadView.setVisibility(View.VISIBLE);
                    hanlderLogin.login(email.getText().toString(),password.getText().toString());

                }
            }
        });

        loginWakecabViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);

            }
        });

        return root;


    }




    public  boolean validatePermission(){
        boolean flags = true;
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            flags = false;
        }
        return flags;
    }

    @Override
    public void showValidationErrorMsg(String msg)
    {
        email.setError(msg);
        password.setError(msg);
        loadView.setVisibility(View.INVISIBLE);

    }
    @Override
    public void loginSuccessFully(String msg)
    {

        if(preDriver.getLoged()){
            View header_name =  navigationView.getHeaderView(0);
            TextView text_header_name = (TextView) header_name.findViewById(R.id.user_name_header);
            text_header_name.setText(preDriver.getNickname());
            TextView text_header_email = (TextView) header_name.findViewById(R.id.user_email_header);
            text_header_email.setText(preDriver.getUseremail());
        }
        Toast.makeText(getContext(), "Login succes "+ msg+"  "+preDriver.getToken(), Toast.LENGTH_SHORT).show();
        this.loadView.setVisibility(View.INVISIBLE);
        Navigation.findNavController(this.getView()).popBackStack();
        Navigation.findNavController(this.getView()).navigate(R.id.nav_home);

    }
    @Override
    public void loginFail(String msg)
    {
        Toast.makeText(getContext(), "No estas registrado ", Toast.LENGTH_SHORT).show();
    }

}