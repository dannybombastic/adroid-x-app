package com.example.wakecab.ui.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class LoginWakecabViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public LoginWakecabViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Welcome to WakeCab.com");
    }

    public LiveData<String> getText() {
        return mText;
    }
}