package com.example.wakecab.ui.table;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.example.wakecab.R;
import com.example.wakecab.ui.home.BaseViewHolder;
import com.example.wakecab.ui.home.TicketsAdapter;
import com.example.wakecab.ui.home.TicketsBean;
import com.example.wakecab.utils.ApiHandler;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.MarkerOptions;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;

public class TableAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private static final String TAG = "TicketsAdapter";


    private List<TicketsBean> mTickettList;
    private LayoutInflater mInflater;
    private Context ctx;
    private TicketsBean.NamedLocation namedLocation;

    private ApiHandler api;


    public TableAdapter(Context ctx, ArrayList<TicketsBean> tk) {
        this.mInflater = LayoutInflater.from(ctx);
        this.mTickettList = tk;
        Log.d("serializando", "serializando");
        this.ctx = ctx;

    }


    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        return new TableAdapter.ViewHolder( LayoutInflater.from(parent.getContext()).inflate(R.layout.item_table, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {


        holder.onBind(position);

    }


    @Override
    public int getItemCount() {
        return mTickettList.size();
    }


    public class ViewHolder extends BaseViewHolder implements View.OnClickListener, OnMapReadyCallback {


        @BindView(R.id.textView_id)
        TextView id;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.carType)
        TextView carType;
        @BindView(R.id.personas)
        TextView personas;
        @BindView(R.id.maletas)
        TextView maletas;
        @BindView(R.id.babySeat)
        TextView babySeat;
        @BindView(R.id.disableSeat)
        TextView disableSeat;
        @BindView(R.id.textView_llegadas)
        TextView llegadas;
        @BindView(R.id.destino)
        TextView destino;
        @BindView(R.id.textView3)
        TextView banner;
        @BindView(R.id.lite_listrow_text)
        TextView bannerMap;



        @BindView(R.id.lite_listrow_map)
        MapView mapView;

        GoogleMap map;
        View layout;


        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView;
            ButterKnife.bind(this, itemView);
            Button button = itemView.findViewById(R.id.button_job);
            button.setOnClickListener(this::onClick);

            if (mapView != null) {
                // Initialise the MapView
                mapView.onCreate(null);
                // Set the map ready callback to receive the GoogleMap object
                mapView.getMapAsync(this);
            }
        }

        @Override
        protected void clear() {

        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onBind(int position) {

            super.onBind(position);

            layout.setTag(this);

            final TicketsBean ticket = mTickettList.get(position);

            namedLocation =  ticket.getNamedLocation();

            mapView.setTag(namedLocation);
            setMapLocation();

            if (ticket.getId() != null) {
                if(ticket.getState().equalsIgnoreCase("vuelta")){

                    llegadas.setText("Vuelta al aueropuerto");
                }
                if(ticket.getState().equalsIgnoreCase("vuelta")){

                    banner.setText("Regreso al aeropuerto");
                }
                id.setText(ticket.getId());


                if (ticket.getDate() != null) {

                    date.setText( ticket.getDate().toLocalDateTime().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)) );

                }
                if (ticket.getCarTipe() != null) {
                    carType.setText(ticket.getCarTipe());
                }
                if (ticket.getMaletas() != null) {
                    maletas.setText(ticket.getMaletas());
                }
                if (ticket.getBabySeat() != null) {
                    babySeat.setText(ticket.getBabySeat());
                }
                if (ticket.getDisable() != null) {
                    disableSeat.setText(ticket.getDisable());
                }
                if (ticket.getPersonas() != null) {
                    personas.setText(ticket.getPersonas());
                }
                if (ticket.getDireccion() != null) {

                    String direccion = ticket.getDireccion();
                    destino.setText(direccion);
                    bannerMap.setText(direccion);
                }
                if (ticket.getPersonas() != null) {
                    maletas.setText(ticket.getMaletas());
                }
                if (ticket.getPersonas() != null) {
                    babySeat.setText(ticket.getBabySeat());
                }




            }


        }


        @Override
        public void onClick(View v) {
            Toast.makeText(ctx,"clickando"+ carType.getText().toString(),Toast.LENGTH_LONG).show();

        }


        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(ctx.getApplicationContext());
            map = googleMap;
            setMapLocation();

        }

        private void setMapLocation() {
            if (map == null) return;

            TicketsBean.NamedLocation data = (TicketsBean.NamedLocation) mapView.getTag();
            if (data == null) return;

            // Add a marker for this item and set the camera
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(data.location, 13f));
            map.addMarker(new MarkerOptions().position(data.location));

            // Set the map type back to normal.
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        }


    }



}
