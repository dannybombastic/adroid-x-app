package com.example.wakecab.utils;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.wakecab.ui.home.TicketsBean;
import com.example.wakecab.ui.login.MySingleton;
import com.example.wakecab.ui.login.WakecabUser;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.O)
public class ApiHandler {
    private Context ctx;
    RequestQueue queue;
    String url_auth = "https://wakecabtest.xyz/api/android/auth/";
    String url_tickets = "https://wakecabtest.xyz/api/android/driver/lista/";
    private Preference_DriverProfile preDriver;
    ArrayList<TicketsBean> tickets = new ArrayList<TicketsBean>();
    AsyncApi asyncApi;
    DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

    public ApiHandler(){

    }
    public ApiHandler(AsyncApi asinc) {
        this.asyncApi = asinc;
    }

    public void getcontext(Context ctx){

        this.ctx = ctx;
        this.preDriver = Preference_DriverProfile.getInstance(ctx);
    }

    public void authGson(final String username, final String password) {
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("username", username);
            jsonBody.put("password", password);
        } catch (JSONException e) {


        }

        MySingleton.getInstance(this.ctx.getApplicationContext()).getRequestQueue();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url_auth, jsonBody, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        preDriver.putWakecabuser(new WakecabUser(username, password));
                        preDriver.putNickname(username);


                        try {
                            preDriver.putToken(response.get("token").toString());
                            preDriver.putVisits(preDriver.getVisits());
                            preDriver.putLoged(true);
                            gettickets();

                        } catch (JSONException e) {
                            Toast.makeText(ctx.getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Toast.makeText(ctx.getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                final Map<String, String> headers = new HashMap<>();
                // headers.put("Authorization", "Token " + "c2FnYXJAa2FydHBheS5jb206cnMwM2UxQUp5RnQzNkQ5NDBxbjNmUDgzNVE3STAyNzI=");//put your token here
                return headers;

            }

        };
        MySingleton.getInstance(ctx).addToRequestQueue(jsonObjectRequest);



    }

    public void gettickets() {

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("user", preDriver.getNickname());

        } catch (JSONException e) {


        }


        JsonObjectRequest jsonArray;
        queue = Volley.newRequestQueue(ctx);

        jsonArray = new JsonObjectRequest(Request.Method.POST, url_tickets, jsonBody,
                new Response.Listener<JSONObject>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONObject response) {
                        // Display the first 500 characters of the response string.


                        try{


                            JSONArray poyo = response.getJSONArray("tickets");

                            for (int i = 0; i < poyo.length(); i++) {
                                JSONObject objects = poyo.getJSONObject(i);
                                Iterator key = objects.keys();


                                        TicketsBean tk = new TicketsBean();

                                        tk.setId(objects.getString("id"));
                                        tk.setDireccion(objects.getJSONObject("client").getJSONObject("client_house").getString("direccion"));
                                        tk.setLat(objects.getJSONObject("client").getJSONObject("client_house").getString("latitude"));
                                        tk.setLng(objects.getJSONObject("client").getJSONObject("client_house").getString("longitude"));
                                        tk.getNamedLocation();
                                        tk.setClient(objects.getJSONObject("client").getString("client_email"));
                                        tk.setDriver(objects.getString("driver"));
                                        tk.setCarTipe(objects.getString("car_type"));
                                        tk.setMaletas(objects.getJSONObject("client").getString("client_suitcase_big"));
                                        tk.setBabySeat(objects.getJSONObject("client").getString("client_kid_check"));
                                        tk.setDisable(objects.getJSONObject("client").getString("client_disable_check"));
                                        tk.setCompany(objects.getString("company"));
                                        tk.setDate( ZonedDateTime.parse(objects.getString("date"), formatter));
                                        tk.setState(objects.getString("tstate"));
                                        tk.setPersonas(objects.getString("personas"));
                                        tickets.add(tk);



                            }

                            Toast.makeText(ctx.getApplicationContext(), "tamaño"+tickets.size()+" ", Toast.LENGTH_LONG).show();
                            asyncApi.tkReady(tickets);
                        }catch (JSONException e){

                            e.printStackTrace();

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }


        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                final Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + preDriver.getToken().toString());

                return headers;

            }
        };

        queue.add(jsonArray);


    }
}
