package com.example.wakecab.utils;

import com.example.wakecab.ui.home.TicketsBean;

import java.util.ArrayList;

public interface AsyncApi {
    void tkReady(ArrayList<TicketsBean> tk);
}