package com.example.wakecab.utils;

import com.example.wakecab.ui.login.WakecabUser;
import com.skydoves.preferenceroom.KeyName;
import com.skydoves.preferenceroom.PreferenceEntity;
import com.skydoves.preferenceroom.PreferenceFunction;
import com.skydoves.preferenceroom.TypeConverter;

@PreferenceEntity(name = "DriverProfile")

public class Profile {

    protected final boolean login = false;
    @KeyName(name = "isactive") protected final boolean isActive = false;
    @KeyName(name = "useremail") protected final String userEmail = null;
    @KeyName(name = "usersurname") protected final String userSurname = null;
    @KeyName(name = "id") protected final String userId = null;
    @KeyName(name = "nickname") protected final String userNickName = null;
    @KeyName(name = "token") protected final String userToken = null;
    @KeyName(name = "loged") protected final boolean userLoged = false;
    @KeyName(name = "visits") protected final int visitCount = 1;

    @KeyName(name = "wakecabuser")
    @TypeConverter(converter = UserConverter.class)
    protected WakecabUser userPetInfo;

    @PreferenceFunction(keyname = "nickname")
    public String putUserNickFunction(String nickname) {
        return   nickname;
    }

    @PreferenceFunction(keyname = "nickname")
    public String getUserNickFunction(String nickname) {
        return nickname;
    }


    @PreferenceFunction(keyname = "token")
    public String putUserTokFunction(String token) {
        return token;
    }

    @PreferenceFunction(keyname = "token")
    public String getUserTokFunction(String token) {
        return token;
    }

    @PreferenceFunction(keyname = "visits")
    public int putVisitCountFunction(int count) {
        return ++count;
    }

    @PreferenceFunction(keyname = "loged")
    public boolean getUserLogFunction(boolean loged) {
        return loged;
    }
    @PreferenceFunction(keyname = "loged")
    public boolean putVisitLogFunction(boolean loged) {
        return loged;
    }

    @PreferenceFunction(keyname = "id")
    public String putUserIdFunction(String id) {
        return id;
    }
    @PreferenceFunction(keyname = "id")
    public String getUserIdFunction(String id) {
        return id;
    }

    @PreferenceFunction(keyname = "usersurname")
    public String putUserSurnameFunction(String userSurname) {
        return userSurname;
    }
    @PreferenceFunction(keyname = "usersurname")
    public String getUserSurnameFunction(String userSurname) {
        return userSurname;
    }

    @PreferenceFunction(keyname = "useremail")
    public String putUserEmailFunction(String userEmail) {
        return userEmail;
    }
    @PreferenceFunction(keyname = "useremail")
    public String getUserEmailFunction(String userEmail) {
        return userEmail;
    }

    @PreferenceFunction(keyname = "isactive")
    public boolean putUserActiveFunction(boolean is_active) {
        return is_active;
    }

    @PreferenceFunction(keyname = "isactive")
    public boolean getUserActiveFunction(boolean isActive) {
        return isActive;
    }




}
