package com.example.wakecab.utils;

import com.example.wakecab.ui.login.WakecabUser;
import com.google.gson.Gson;
import com.skydoves.preferenceroom.PreferenceTypeConverter;

public class UserConverter extends PreferenceTypeConverter<WakecabUser> {

    private final Gson gson;

    public UserConverter(Class<WakecabUser> clazz) {
        super(clazz);
        this.gson = new Gson();
    }


    public String convertObject(WakecabUser object) {

        return gson.toJson(object);
    }

    @Override
    public WakecabUser convertType(String string) {
        return gson.fromJson(string, WakecabUser.class);
    }
}
